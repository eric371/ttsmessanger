import * as Resource from '@/resource'

var cache = new Resource.Cache(15)
var history = new Resource.History(5)
var audio = new Audio()
var playAudioOnce = function(url) {
  if(audio.src!="") {
    audio.pause;
    //URL.revokeObjectURL(vm.audio.src)
  }
  audio.src = url
  audio.addEventListener('loadeddata', () => {
    audio.play()
  })
  audio.addEventListener('ended', () => {
    URL.revokeObjectURL(url)
  })
}
export {
  cache,
  history,
  audio,
  playAudioOnce
}
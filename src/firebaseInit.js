import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/functions'

const firebaseConfig = { 
  apiKey: "AIzaSyBG0weAH2gmDp2LPEeetIqoyMzF8iIcMgk",
  authDomain: "keen-client-305309.firebaseapp.com",
  projectId: "keen-client-305309",
  storageBucket: "keen-client-305309.appspot.com",
  messagingSenderId: "1093249088216",
  appId: "1:1093249088216:web:abde6eb240094405af75ad"
};
//{
  /*put in firebase config*/
//};

firebase.initializeApp(firebaseConfig)
firebase.firestore().useEmulator("localhost",5002)
firebase.functions().useEmulator("localhost", 5001);
firebase.auth().useEmulator("http://localhost:9099")

const firebaseServerLocation = 'us-central1'

const connectionRequestURL = 'http://localhost:5001/'+firebaseConfig.projectId+'/'+firebaseServerLocation+'/connectionRequest'

export {firebaseConfig, firebaseServerLocation, connectionRequestURL}

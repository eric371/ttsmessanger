function ResourceRef(URL) {
  this.refcount = 0
  this.url = URL
}
ResourceRef.prototype.addRef = function () {
  this.refcount ++;
}
ResourceRef.prototype.delRef = function() {
  this.refcount --;
  if(this.refcount <= 0) {
    URL.revokeObjectURL(this.url)
  }
}
function ResourceCache(size = 15) {
  this.size = size
  this.cache = new Map
}
ResourceCache.prototype.add = function (key, val) {
  val.addRef();
  let get = this.cache.get(key)
  if(get!=undefined) {
    get.delRef()
    this.cache.delete(key);
  }
  this.cache.set(key, val)
  if(this.cache.size > this.size) {
    let it = this.cache.entries()
    let k,v = it.next() // delete first
    v.delRef()
    this.cache.delete(k)
  }
}
ResourceCache.prototype.get = function(key) {
  if(this.cache.has(key)) {
    let v = this.cache.get(key);
    this.cache.delete(key);
    this.cache.set(key,v)
    return v
  }
  return undefined
}
function ResourceHistory(size = 5) {
  this.size = size
  this.array = []
  this.head = 0
}
ResourceHistory.prototype.add = function(sentence, resource) {
  resource.addRef()
  let obj = {message: sentence, resource: resource, id: this.head}
  if(this.array.length< this.size) {
    this.array.push(obj); 
  } else {
    this.array[this.head].resource.delRef()
    this.array.splice(this.head,1,obj)
  }
  this.head =(this.head+1)%this.size
}

export {
  ResourceRef as Ref,
  ResourceCache as Cache,
  ResourceHistory as History
}
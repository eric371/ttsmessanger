import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/functions'
import Peer from 'simple-peer'
import * as Audio from '@/audio'
import {peerState} from '@/peer'
import {connectionRequestURL} from '@/firebaseInit'

Vue.use(Vuex)

firebase.auth().onIdTokenChanged((user) => {
  store.commit('setUserData', user)
  if (user) {
    user.getIdToken().then((accessToken) => {
      store.commit('setToken', accessToken)
    });
    user.getIdTokenResult()
    .then((idTokenResult) => {
      if(idTokenResult.claims.superuser == true) {
        store.commit('setIsSuperUser', true)
      } else {
        store.commit('setIsSuperUser', false)
      }
    })    
  } else {
    store.commit('setToken', null)
  }
}, (error) => {
  console.log(error);
});

const vuexLocalStorage = new VuexPersist({
  key: 'vuex',
  storage: window.localStorage,
  reducer: (state)=>{return {voiceOptionsEntry: state.voiceOptionsEntry}}
})

var connectionRequestUnsub = null
const store = new Vuex.Store({
  plugins: [vuexLocalStorage.plugin],
  state: {
    connectionStatus: 'disconnected',
    connectionInfo: '',
    closedConnectionMyself: false,
    targetUser: '',

    user: null,
    token: null,
    isSuperUser: true,
    voiceOptionsEntry: {voiceOptions: [], created: 0},
  },
  mutations: {
    setUserData(state, val) {
      state.user = val
    },
    setToken(state, val) {
      state.token = val
    },
    setIsSuperUser(state, val) {
      state.isSuperUser = val
    },
    setVoiceOptions(state, val) {
      state.voiceOptionsEntry = {voiceOptions: val, created: Date.now()}
    },
    setConnectionStatus(state, val) {
      state.connectionStatus = val
    },
    setConnectionInfo(state, val) {
      state.connectionInfo = val
    },
    setClosedConnectionMyself(state, val) {
      state.closedConnectionMyself = val
    },
    setTargetUser(state, val) {
      state.targetUser = val
    }
  },
  actions: {
    checkedGetVoiceOptions({commit, state}) {
      if(
        state.voiceOptionsEntry.voiceOptions.length == 0 || 
        Date.now() - state.voiceOptionsEntry.created > 1000*60*60*24 
      ) {
        var getVoices = firebase.functions().httpsCallable('getVoices');
        getVoices()
        .then((response) => {
          let i = 0
          let vo = []
          for(let voice of response.data.voices) {
            let opt = {id: i, name: voice.name, gender: voice.ssmlGender, samplerate: voice.naturalSampleRateHertz, languageCode:voice.languageCodes[0]}
            vo.push(opt)
            i++
          }
          vo.sort((a,b)=>{return a.name>b.name?1:-1})
  
          commit('setVoiceOptions', vo)
        })
        .catch(()/*error*/ => {
          return Promise.resolve([])
        });
      } else {
        return Promise.resolve(state.voiceOptionsEntry.voiceOptions)
      }
    },
    async connectionRequest({commit, state}, type) {
      if(type == 'initiate') {
        commit('setConnectionStatus', 'initiated')
      }
      if(peerState.peer != null) {
        peerState.peer.destroy()
      }
      peerState.peer = new Peer({initiator: type == 'initiate', trickle: false})
      
      peerState.peer.on('signal', async peerData => {
        await fetch(connectionRequestURL, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer'+state.token
          },
          body: JSON.stringify({type: type, target: state.targetUser, signal: JSON.stringify(peerData)})
        })
      })
      if(type=="respond") {
        peerState.peer.signal(peerState.incomingSignal)
      }
      peerState.peer.on('connect', () => {
        commit('setConnectionStatus', 'connected')
        commit('setConnectionInfo', '')
      })
      peerState.peer.on('close', () => {
        commit('setConnectionStatus', 'disconnected')
        if(!state.closedConnectionMyself) {
          commit('setConnectionInfo', 'Connection closed by peer')
        } else {
          commit('setClosedConnectionMyself', false)
          //commit('setConnectionInfo', '')
        }
      })
      peerState.peer.on('data', (data) => {
        Audio.playAudioOnce(URL.createObjectURL(new Blob([data.buffer], {type: "audio/wav"})))
      })
      peerState.peer.on('error', (err) => {
        console.log("Peer error: "+err)
      })
    },
    async cancelRequest({state, dispatch}) {
      dispatch('disconnectPeer')
      await fetch(connectionRequestURL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer'+state.token
        },
        body: JSON.stringify({type: "cancel", target: state.targetUser})
      })
    },
    disconnectPeer({commit}) {
        commit('setConnectionStatus', 'disconnected')
        if(peerState.peer != null) {
          commit('setClosedConnectionMyself', true)
          peerState.peer.destroy()
        } 
    },
    async onLogIn({commit,dispatch,state}) {
      connectionRequestUnsub = firebase.firestore().collection('connectionRequest').doc(firebase.auth().currentUser.uid).onSnapshot((doc) => {
        let data = doc.data()
        if(data == undefined) {return}
        if(data.type == "cancel") {
          if(state.connectionStatus != "disconnected" ) {
            if(state.connectionStatus == "initiated") {
              commit('setConnectionInfo', 'Connection attempt declined by peer')  
            } else if (state.connectionStatus == "deciding") {
              commit('setConnectionInfo', 'Connection attempt canceled by peer')
            }
            dispatch('disconnectPeer')
          }
        }
        else if(state.connectionStatus == "initiated" && data.type == "respond") {
          if(data.source == state.targetUser) {
            //commit('setConnectionStatus', 'responded')
            peerState.peer.signal(JSON.parse(data.signal))
          }
        } else if (state.connectionStatus == "disconnected"&& data.type == "initiate") {
          commit('setTargetUser', data.source)
          peerState.incomingSignal =  data.signal
          commit('setConnectionStatus', 'deciding')
          commit('setConnectionInfo', '')
        }
      })
      dispatch('checkedGetVoiceOptions')
    },
    async onLogOff() {
      if(connectionRequestUnsub) {
        connectionRequestUnsub()
      }
    },
  },
  modules: {
  },
  getters: {
    getVoiceOptions(state) {
      return state.voiceOptionsEntry.voiceOptions

    }
  }
})

export default store
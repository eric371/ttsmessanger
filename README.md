# ttsmessanger

Firebase project which lets people communicate p2p with synthesized speech.
It is set up to work with the emulator suit.
You need to put your firebase config in firebaseInit.js first

## Project setup uses vue-cli
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```


const functions = require("firebase-functions");
const admin = require("firebase-admin");
const textToSpeech = require("@google-cloud/text-to-speech");
// const {Message} = require("firebase-functions/lib/providers/pubsub");
admin.initializeApp();
const cors = require("cors")({origin: true});

exports.connectionRequest = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    let token = req.get("authorization");
    try {
      if (token == undefined) {
        throw new Error;
      }
      // remove "bearer"
      token = await admin.auth().verifyIdToken(token.slice(6));
    } catch (err) {
      res.send({status: "error", reason: "not authorized"});
      return;
    }

    let target = req.body.target;
    target = await admin.auth().getUserByEmail(target);
    let data;
    if (req.body.type == "cancel") {
      data = {
        "type": req.body.type,
        "source": token.email,
        "timestamp": Date.now(),
        // timestamped so snapshot handler triggers 
        // (must be different data)
      };
    } else {
      data = {
        "type": req.body.type,
        "source": token.email,
        "signal": req.body.signal,
        "timestamp": Date.now(),
      };
    }
    await admin.firestore().collection("connectionRequest")
        .doc(target.uid).set(data);
    res.send({status: "ok"});
  })
})

exports.initUserDatabase = functions.auth.user().onCreate((user) => {
  admin.firestore().collection("user").doc(user.uid).set({contingent: 100});
  return user
});

exports.getVoices = functions.https.onCall((data, context) => {
  if (context.auth == null) {
    throw new Error("not signed in")
  }
  const client = new textToSpeech.TextToSpeechClient();
  return client.listVoices().then((result) => result[0])
})

exports.synthesize = functions.https.onCall(async (data, context) => {
  if (context.auth == null) {
    throw new Error("not signed in")
  }
  let textlen = data.input.text.length;
  if (context.auth.token.superuser == true) {
    textlen = 0
  }
  const ref = admin.firestore().collection("user").doc(context.auth.uid);
  let newContingent = null;
  newContingent = await admin.firestore().runTransaction(
      async (transaction) => {
        const doc = await transaction.get(ref)
        if (doc.data().contingent < textlen) {
          throw new Error("contingent insufficient")
        } else {
          const newContingent = doc.data().contingent-textlen
          await transaction.update(ref, {contingent: newContingent})
          return newContingent
        }
      },
  )
  const client = new textToSpeech.TextToSpeechClient();
  const [response] = await client.synthesizeSpeech(data);
  return {audioContent: response.audioContent, contingent: newContingent}
})

exports.validateKey = functions.https.onCall((data, context) => {
  const key = data.key;
  if (context.auth == null) {
    throw new Error("no token")
  }
  const uid = context.auth.uid;
  if (key == "supersecret") {
    admin.auth().setCustomUserClaims(uid, {superuser: true})
    return {}
  } else {
    throw new Error("not authorized")
  }
});
